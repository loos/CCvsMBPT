\documentclass[10pt]{letter}
\usepackage{UPS_letterhead,xcolor,mhchem,ragged2e,hyperref}
\newcommand{\alert}[1]{\textcolor{red}{#1}}
\definecolor{darkgreen}{HTML}{009900}


\begin{document}

\begin{letter}%
{To the Editors of the Journal of Chemical Physics,}

\opening{Dear Editors,}

\justifying
Please find attached a revised version of the manuscript entitled 
\begin{quote}
	\textit{``Connections between many-body perturbation and coupled-cluster theories''}.
\end{quote}
We thank the reviewers for their constructive comments and to support publication of the present manuscript.
Our detailed responses to their comments can be found below.
For convenience, changes are highlighted in red in the revised version of the manuscript. 

We look forward to hearing from you.

\closing{Sincerely, the authors.}

\newpage

%%% REVIEWER 1 %%%
\noindent \textbf{\large Authors' answer to Reviewer \#1}



{In this manuscript, Quintero-Monsebaiz et al. expand upon previously established connections between coupled-cluster (CC) theory and the random-phase approximation. 
Specifically, they aim to connect CC theory to the Bethe-Salpeter equation (BSE) for neutral excitations and to the $GW$ approximation for charged (IP/EA) excitations, both of which have RPA screening at their heart. 
The manuscript is well-written and contributes to a growing body of literature comparing, theoretically and numerically, the performance of wavefunction and Green's function-based many-body theories. 
J. Chem. Phys. is a good venue for this work, and I am happy to recommend publication, although I have a few suggestions for the authors to consider.
}
\\
\alert{
We thank the reviewer for supporting the publication of the present manuscript.
Below, we address his/her suggestions.
}

\begin{enumerate}

\item 
{How should I understand the BSE correlation energy and the plasmon trace formula given in Eq.~(9)? 
The BSE is an approximation to the two-particle Green's function, which completely determines the two-particle reduced density matrix. 
From this, I could obtain the total energy by contraction with one- and two-electron Hamiltonian matrix elements. 
Can this viewpoint be related precisely to the trace formula (9)? 
If so, I think a few words to make this connection would be valuable to the reader. 
It's not obvious (to me at least) that all methods that allow a Tamm-Dancoff approximation can rigorously be related to a correlation energy expression in the form of the plasmon trace formula.}
\\
\alert{
Like in the case of RPA, there are many ways of defining the correlation energy at the BSE level (as mentioned in the original manuscript).
The trace formula (9) is one of them and it has been recently used to compute the potential energy surfaces of diatomics [see Refs.~(62) and (63)].
Another way is to rely on the adiabatic-connection fluctuation-dissipation theorem as done by Holzer et al.~[see Ref.~(37)] and some of the authors [see Ref.~(38)]. 
\\
To the best of our knowledge, the trace (or plasmon) formula has been introduced first by Sawada to calculate the correlation energy of the uniform electron gas as an alternative to the Gell-Mann \& Brueckner formula which integrates along the adiabatic connection path.
These two approaches were later found to be equivalent in the case of direct RPA for both homogeneous and inhomogeneous systems.
\\
More precisely, the trace formula can be justified via the introduction of a quadratic Hamiltonian made of boson transition operators (quasiboson approximation) as discussed in Refs.~(62).
A footnote gathering these information has been added to the revised version of the manuscript alongside additional references.
}

\item 
{Concerning the discussion below Eq.~(23): I note that the ``$GW$ pre-treatment'' renormalizes the one-electron energies via more than mosaic diagrams. 
The mosaic diagrams are the only ones that are captured at the CCSD level, but in general, the $GW$ approximation also has a forward-time-ordered screened exchange interaction mediated by particle-hole pairs (requiring more than CCSD).
}
\\
\alert{
The reviewer is right.
We have modified this statement as ``The $GW$ pre-treatment renormalizes the bare one-electron energies and, consequently, incorporates mosaic as well as additional diagrams, a process named Brueckner-like dressing in Ref.~46.''
}

\item 
{In general, Eq.~(24) defines all poles of the one-particle Green's function, although the surrounding discussion suggests that only quasiparticle excitations are found in this manner. 
(I'm sure the authors appreciate this point, as made clear elsewhere in the manuscript).
}
\\
\alert{
The reviewer is right again. We have mentioned this just below Eq.~(24).
}

\item 
{I quite like the discussion beginning with Eq.~(29), but it makes me wonder about the major findings of the present manuscript. 
Specifically, I think this manuscript exploits the following numerical observation. 
Consider any eigenvalue problem and partition it into two spaces,
\begin{equation}
	\begin{pmatrix}
		\boldsymbol{A}	&	\boldsymbol{V}	\\
		\boldsymbol{V}^T	&	\boldsymbol{B}	\\
	\end{pmatrix}
	\begin{pmatrix}
		\boldsymbol{X}	\\
		\boldsymbol{Y}	\\
	\end{pmatrix}
	=
	\begin{pmatrix}
		\boldsymbol{X}	\\
		\boldsymbol{Y}	\\
	\end{pmatrix}
	\boldsymbol{\Omega}
\end{equation}
Assuming existence of the inverse, this can be trivially rewritten as an eigenvalue problem for a matrix in the $\boldsymbol{A}$ subspace,
\begin{equation}
	\boldsymbol{A} + \boldsymbol{V} \boldsymbol{T}
\end{equation}
where $\boldsymbol{T} = \boldsymbol{Y} \boldsymbol{X}^{-1}$. Of course, this is not useful without $\boldsymbol{T}$. So what determines $\boldsymbol{T}$ without access to all the eigenvectors contained in $\boldsymbol{X}$, $\boldsymbol{Y}$? 
We can rewrite the two equations contained in (1) as 
\begin{gather}
	\boldsymbol{A} + \boldsymbol{V} \boldsymbol{T} = \boldsymbol{X} \boldsymbol{\Omega} \boldsymbol{X}^{-1}
	\boldsymbol{V}^T + \boldsymbol{B} \boldsymbol{T} = \boldsymbol{T} \boldsymbol{X} \boldsymbol{\Omega} \boldsymbol{X}^{-1} 
\end{gather}
or, upon combining the above equations,
\begin{equation}
	\boldsymbol{V}^T + \boldsymbol{B} \boldsymbol{T} - \boldsymbol{T} \boldsymbol{A} - \boldsymbol{T} \boldsymbol{V} \boldsymbol{T} = \boldsymbol{0}
\end{equation}
The above is some nonlinear equation that can be solved for the unknown $\boldsymbol{T}$. 
It looks superficially like a CCSD equation (which could be solved, e.g., by iteration), but clearly there is no formal connection. 
Does the fact that any linear eigenvalue equation can be partitioned to produce a nonlinear eigenvalue equation, or equivalently recast into the determination of the solution to a system of nonlinear equations, imply any formal connection with CC theory (as the section title implies)? 
Or is it a mathematically interesting observation that indicates an alternative route toward the determination of eigenvalues from a partitioned matrix?
As far as I can tell, the above general observation is applied in two distinct settings to derive the results in the present manuscript. 
For the BSE, the partitioning is used to eliminate the ``deexcitation'' space, similar to its elimination in the RPA problem. 
For the $GW$ approximation, the partitioning is used to eliminate the 2h1p/2p1h space. 
However, I reemphasize that in both cases, the ``CC-like'' equations that result are analogous to ground-state CC theory, and so the formal connection between the theories is tenuous.
}
\\
\alert{
The reviewer is right in some aspects but we believe that the connection between $GW$ and CC is more profound and this is nicely illustrated by the work of Lange and Berkelbach [see Ref.~(72)], where they show diagrammatic connections between $GW$ and approximate EOM-CCD.
}

\item 
{A confusion I have in my own discussion above is related to a question I have about the final Eq.~(34) of the present work. 
Where was the approximation made that indicates that this only finds the $N$ principle excitation energies? 
I agree that $\epsilon + \Sigma$ is only $N \times N$ so it should only have $N$ eigenvalues. 
But all equations were exact, assuming existence of the inverse. 
What am I missing here? 
If the authors appreciate this confusion, then some discussion in the manuscript would be welcome.}
\\
\alert{
This is explicitly stated before Eq.~(29) as ``Let us suppose that we are looking for the $N$ ``principal'' (i.e., quasiparticle) solutions of the eigensystem (26).''
However, we understand the confusion of the reviewer.
Let us be more precise.
The iterative algorithm proposed here [see Eqs.~(33)] could potentially converge to satellite solutions, and this is also the case at the CC level when one relies on a Newton-Raphson algorithm to converge to higher-energy solutions by modifying the updating procedure in Eqs.~(33) [see JCTC 17 (2021) 4756 and references therein].
We discuss further this point below Eq.~(34).
}

\end{enumerate}

%%% REVIEWER 2 %%%
\noindent \textbf{\large Authors' answer to Reviewer \#2}

{This manuscript builds on recent analytic work, drawing connections and deepening understanding between different electronic structure methods, specifically for excitations. 
The work is self-contained, coherent and concise. 
I cannot fault the presentation of the analytic results, their correctness, or the interpretation and understanding that can be drawn from them. 
I would recommend acceptance without reservation.}
\\
\alert{
We thank the reviewer for these kind comments and supporting publication of the present Communication.
}


\end{letter}
\end{document}
